const path = require("path");
const common = require('./webpack.common');
const {merge} = require('webpack-merge');
const {CleanWebpackPlugin} = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = merge(common, {
    mode: 'production',
    output: {
        filename: '[name].[contenthash].bundle.js',
        path: path.resolve(__dirname, "dist"),
        assetModuleFilename: 'images/[name][hash][ext][query]'
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].[contenthash].css'
        }),
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: './src/template.html',
            inject: 'body',
            // minify: {
            //     collapseWhitespace: true,
            //     removeComments: true
            // }
        })
    ],
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/,
                use: [
                    MiniCssExtractPlugin.loader,     //3. Extract css into files
                    'css-loader',       //2. Translates CSS into CommonJS
                    'sass-loader'       //1. Combines Sass to CSS
                ]
            },
        ]
    },
    optimization: {
        minimizer: [new OptimizeCssAssetsPlugin() ] //, new TerserPlugin()]
    }
});