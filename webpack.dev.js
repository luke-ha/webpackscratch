const path = require("path");
const common = require('./webpack.common');
const {merge} = require('webpack-merge');
var HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = merge(common, {
    mode: 'development',
    // output: {
    //     filename: '[name].bundle.js',
    //     // path: path.resolve(__dirname, "dist"),
    //     assetModuleFilename: 'images/[name][hash][ext][query]'
    // },
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/,
                use: [
                    'style-loader',     //3. Creates `style` nodes from JS strings-
                    'css-loader',       //2. Translates CSS into CommonJS
                    'sass-loader'       //1. Combines Sass to CSS
                ]
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/template.html',
            inject: 'body'
        })
    ]
});