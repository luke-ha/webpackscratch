export const inputsAreValid = (...inputs) => {
    return inputs.every(num => !isNaN(num) && num != '');
}