import {inputsAreValid} from './units/inputs-are-valid'
export class AlertService {
    constructor() {
        this.errorBox = document.getElementById('error');
    }

    handleAdditionError(inputs){
        const fullResult = inputs.reduce((result, input, index, array)=> {
            if(inputsAreValid(input)) {
                return result + '';
            } else {
                return result + `'${input}' is not a number. `
            }
        }, 'Please enter two valid numbers!');

        this.errorBox.innerText = fullResult;
    }

    hideErrors() {
        this.errorBox.innerText = '';
    }
}