export class ComponentService {
    constructor() {
        this.numberOneInput = document.getElementById('numberOne');
        this.numberTwoInput = document.getElementById('numberTwo');
        this.calculateButton = document.getElementById('calculate');
        this.resultBox = document.getElementById('result');
    }

    getInputs() {
        return [this.numberOneInput.value, this.numberTwoInput.value];
    }

    setResult(str) {
        this.resultBox.innerText = str;
    }

    onClick(cb) {
        this.calculateButton.addEventListener("click", cb);
    }
}